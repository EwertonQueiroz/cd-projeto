module bancoRegistradores(rst, clk, wrEn, addR1, addR2, addWr, dadoR1, dadoR2, dadoWr);
	input clk, rst, wrEn;
	input [2:0] addR1, addR2, addWr;
	input [7:0] dadoWr;
	output reg [7:0] dadoR1; 
	output reg [7:0] dadoR2;

	reg [7:0] dadoreg [0:7];
	always@ (posedge clk or negedge rst) begin

		if(rst == 0) begin
			dadoreg[0] <= 8'h0;
			dadoreg[1] <= 8'h1;
			dadoreg[2] <= 8'h2;
			dadoreg[3] <= 8'h3;
			dadoreg[4] <= 8'h4;
			dadoreg[5] <= 8'h5;
			dadoreg[6] <= 8'h6;
			dadoreg[7] <= 8'h7;
		end
		
		else begin
			dadoR1  <= dadoreg[addR1];
			dadoR2  <= dadoreg[addR2];
			
			if(wrEn) begin
				dadoreg[addWr] <= dadoWr;
			end
		end
	end
endmodule
